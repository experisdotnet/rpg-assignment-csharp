﻿using RPG_Assigment_csharp.Characters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.CompilerServices;
using System.Diagnostics;
using RPG_Assigment_csharp.Interfaces;

namespace RPG_Assigment_csharp.Stats
{
    public class BasePrimaryAttributes : ITriStats
    {

        public double Strength { get;  set; }
        public double Dexterity { get;  set; }
        public double Intelligence { get;  set; }


    }
}
