﻿using RPG_Assigment_csharp.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Assigment_csharp.Stats
{
    public class ArmorAttrClass: ITriStats
    {

        public double Strength { get; set; }
        public double Dexterity { get; set; }
        public double Intelligence { get; set; }


    }
}
