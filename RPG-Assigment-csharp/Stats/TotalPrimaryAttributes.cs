﻿using RPG_Assigment_csharp.Characters;
using RPG_Assigment_csharp.Hero;
using RPG_Assigment_csharp.Interfaces;
using RPG_Assigment_csharp.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Assigment_csharp.Stats
{
    public class TotalPrimaryAttributes : ITriStats
    {

        public double Strength { get; set; }
        public double Dexterity { get; set; }
        public double Intelligence { get; set; }
      
        
        
        

        public TotalPrimaryAttributes()
        {

        }


    }
}
