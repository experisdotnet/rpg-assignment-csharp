﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Assigment_csharp.Exceptions
{
    public class InvalidArmorException : Exception // Custom exception if invalid armor 
    {
        public InvalidArmorException(string message) : base(message)
        {

        }

        public override string Message => "Invalid armor exception";
    }
}
