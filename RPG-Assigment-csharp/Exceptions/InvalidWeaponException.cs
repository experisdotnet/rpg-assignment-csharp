﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Assigment_csharp.Exceptions
{
    public class InvalidWeaponException : Exception // Custom exception if invalid weapon
    {
        public InvalidWeaponException(string message) : base(message)
        {

        }

        public override string Message => "Invalid weapon exception";
    }
}
