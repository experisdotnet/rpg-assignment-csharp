﻿using RPG_Assigment_csharp.Enums;
using RPG_Assigment_csharp.Exceptions;
using RPG_Assigment_csharp.Hero;
using RPG_Assigment_csharp.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Assigment_csharp.Characters
{
    public class RogueClass : HeroClass
    {
        public double Damage { get; set; }

        private Dictionary<SlotEnum, ItemClass> inventory = new Dictionary<SlotEnum, ItemClass>();

        public Dictionary<SlotEnum, ItemClass> Inventory
        {
            get { return inventory; }
            private set { inventory = value; } // private set
        }

        public RogueClass() : base()
        {
        }

        #region Equip item Rogue method
        /// <summary>
        /// Equips item for rogue, required to pass item, slot and instance of rogue
        /// </summary>
        /// <param name="item">Which item is beeing passed in</param>
        /// <param name="slot">Wich slot is beeing passed in</param>
        /// <param name="rogue">Which instance is beeing passed in</param>
        /// <returns></returns>
        /// <exception cref="InvalidArmorException">If not possible to equip that type of armor or beeing to low level for that armor</exception>
        /// <exception cref="InvalidWeaponException">If not possible to equip that type of weapon or beeing to low level for that weapon</exception>
        public string EquipRogueItem(ItemClass item, SlotEnum slot, RogueClass rogue)
        {
            string notification = ""; // initialize notification with empty string


            // check if item is already in slot and remove if it is
            if (Inventory.ContainsKey(slot) && slot.Equals(SlotEnum.SLOT_BODY))
            {
                Inventory.Remove(slot);
            }
            if (Inventory.ContainsKey(slot) && slot.Equals(SlotEnum.SLOT_LEGS))
            {
                Inventory.Remove(slot);
            }
            if (Inventory.ContainsKey(slot) && slot.Equals(SlotEnum.SLOT_HEAD))
            {
                Inventory.Remove(slot);
            }
            if (Inventory.ContainsKey(slot) && slot.Equals(SlotEnum.SLOT_WEAPON))
            {
                Inventory.Remove(slot);

            }

            if (item.GetType() == typeof(ArmorClass)) // check if item is of type ArmorClass
            {
                ArmorClass armor = (ArmorClass)item; // Cast item to ArmorClass

                //Check for requirements
                if (GetType() == typeof(RangerClass) && armor.ArmorType == ArmorEnum.ARMOR_LEATHER && rogue.Level >= armor.ReqLvl ||
                    GetType() == typeof(RangerClass) && armor.ArmorType == ArmorEnum.ARMOR_MAIL && rogue.Level >= armor.ReqLvl)
                {
                    // If slot is of type weapon, throw exception
                    if (slot == SlotEnum.SLOT_WEAPON)
                    {
                        throw new Exception("Not the right slot for an armor");
                    }
                    else // add item to inventory
                    {
                        Inventory.Add(slot, item);
                        notification = "New armour equipped!";
                    }
                }
                else // throw custom exception if requirements not met
                {
                    throw new InvalidArmorException("Not possible to equip this type of armor");
                }

            }

            if (item.GetType() == typeof(WeaponClass)) // check if item is of type WeaponClass
            {
                WeaponClass weapon = (WeaponClass)item; // cast item to typ WeaponClass

                //Check for requirements
                if ((GetType() == typeof(RogueClass) && weapon.WeaponType == WeaponsEnum.WEAPON_DAGGER && rogue.Level >= weapon.ReqLvl ||
                    GetType() == typeof(RogueClass) && weapon.WeaponType == WeaponsEnum.WEAPON_SWORD) && rogue.Level >= weapon.ReqLvl)
                {
                    // If slot is not of weapon throw exception
                    if (weapon.Slot != SlotEnum.SLOT_WEAPON)
                    {
                        throw new Exception("Not the right slot for a weapon");
                    }
                    else // Add item to inventory
                    {
                        Inventory.Add(slot, item);
                        notification = "New weapon equipped!";

                    }
                }
                else // throw exception if requirements not met
                {
                    throw new InvalidWeaponException("Not possible to equip this weapon");
                }
                
            }
            return notification; // return notification
        }

        #endregion



        #region total variables with armor stats
        /// <summary>
        /// For every item in rogue.inventory add armor attributes to total stats,
        /// if level is one add base stats 
        /// </summary>
        /// <param name="rogue">Which instance of rogue is beeing passed in</param>
        /// <exception cref="Exception">Throws exception if wrong type is passed in</exception>
        public void TotalVariablesWithArmorStats(RogueClass rogue)
        {

                foreach (var item in rogue.Inventory) // loop through inventory
                {
                    if (item.Value.GetType() == typeof(ArmorClass)) // check if item equals type ArmorClass
                    {
                        ArmorClass armorClass = (ArmorClass)item.Value; // cast item to ArmorClass

                        // Add total primarity values with attributes from armor
                        rogue.TotalPrimaryAttributes.Strength += armorClass.Attributes.Strength;
                        rogue.TotalPrimaryAttributes.Dexterity += armorClass.Attributes.Dexterity;
                        rogue.TotalPrimaryAttributes.Intelligence += armorClass.Attributes.Intelligence;

                    }

                }

                if (rogue.Level >= 1)
                {
                    //Add base primary attributes to total attributes
                    rogue.TotalPrimaryAttributes.Strength += rogue.BasePrimaryAttributes.Strength;
                    rogue.TotalPrimaryAttributes.Dexterity += rogue.BasePrimaryAttributes.Dexterity;
                    rogue.TotalPrimaryAttributes.Intelligence += rogue.BasePrimaryAttributes.Intelligence;
                }


        }

        #endregion
    }


}



