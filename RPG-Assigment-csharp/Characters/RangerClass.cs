﻿using RPG_Assigment_csharp.Enums;
using RPG_Assigment_csharp.Exceptions;
using RPG_Assigment_csharp.Hero;
using RPG_Assigment_csharp.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Assigment_csharp.Characters
{
    public class RangerClass : HeroClass
    {
        public double Damage { get; set; }


        private Dictionary<SlotEnum, ItemClass> inventory = new Dictionary<SlotEnum, ItemClass>();
        public Dictionary<SlotEnum, ItemClass> Inventory
        {
            get { return inventory; }
            private set { inventory = value; } // private set
        }
        public RangerClass() : base()
        {
        }


        #region Equip item Ranger method
        /// <summary>
        /// Equips item for ranger, required to pass item, slot and instance of ranger
        /// </summary>
        /// <param name="item">Which item is beeing passed in</param>
        /// <param name="slot">Wich slot is beeing passed in</param>
        /// <param name="ranger">Which instance is beeing passed in</param>
        /// <returns></returns>
        /// <exception cref="InvalidArmorException">If not possible to equip that type of armor or beeing to low level for that armor</exception>
        /// <exception cref="InvalidWeaponException">If not possible to equip that type of weapon or beeing to low level for that weapon</exception>
        public string EquipRangerItem(ItemClass item, SlotEnum slot, RangerClass ranger)
        {
            string notification = ""; // initialize variable


            //checks if inventory contains any equipped items and removes before equipping new item
            if (Inventory.ContainsKey(slot) && slot.Equals(SlotEnum.SLOT_BODY))
            {
                Inventory.Remove(slot);
            }
            if (Inventory.ContainsKey(slot) && slot.Equals(SlotEnum.SLOT_LEGS))
            {
                Inventory.Remove(slot);
            }
            if (Inventory.ContainsKey(slot) && slot.Equals(SlotEnum.SLOT_HEAD))
            {
                Inventory.Remove(slot);
            }
            if (Inventory.ContainsKey(slot) && slot.Equals(SlotEnum.SLOT_WEAPON))
            {
                Inventory.Remove(slot);

            }

           if (item.GetType() == typeof(ArmorClass)) // Checks item is of type ArmorClass
            {
                ArmorClass armor = (ArmorClass)item; // casts item to ArmorClass

                //checks if requirements for equipping item is met
                if (GetType() == typeof(RangerClass) && armor.ArmorType == ArmorEnum.ARMOR_LEATHER && ranger.Level >= armor.ReqLvl ||
                    GetType() == typeof(RangerClass) && armor.ArmorType == ArmorEnum.ARMOR_MAIL && ranger.Level >= armor.ReqLvl)
                {
                    // throw exception if slot is set to weapon
                    if (slot == SlotEnum.SLOT_WEAPON)
                    {
                        throw new Exception("Not the right slot for an armor");
                    }
                    else // Add item to inventory if not weapon slot
                    {
                        Inventory.Add(slot, item);
                        notification = "New armour equipped!";
                    }
                }
                else // throw custom exception while not meeting requirements for equipping item
                {
                    throw new InvalidArmorException("Not possible to equip this type of armor");
                }
            }

            if (item.GetType() == typeof(WeaponClass)) // check if item is of type WeaponClass
            {
                WeaponClass weapon = (WeaponClass)item; // Cast item to WeaponClass
                
                // Check if requirements for equipping weapon is met
                if (weapon.WeaponType.Equals(WeaponsEnum.WEAPON_BOW) && ranger.Level >= weapon.ReqLvl)
                {
                    if (weapon.Slot != SlotEnum.SLOT_WEAPON)
                    {
                        throw new Exception("Not the right slot for a weapon");
                    }
                    else
                    {
                        Inventory.Add(slot, item); // add item to slot
                        notification = "New weapon equipped!";
                    }
                }
                else // throw custom exception
                {
                    throw new InvalidWeaponException("Not possible to equip this weapon"); 
                }
                
            }
            return notification;
        }

        #endregion


        #region Total variables with armor stats
        /// <summary>
        /// For every item in ranger.inventory add armor attributes to total stats,
        /// if level is one add base stats 
        /// </summary>
        /// <param name="ranger">Which instance of ranger is beeing passed in</param>
        /// <exception cref="Exception">Throws exception if wrong type is passed in</exception>
        public void TotalVariablesWithArmorStats(RangerClass ranger)
        {

                foreach (var item in ranger.Inventory) // Loop trough inventory
                {
                    if (item.Value.GetType() == typeof(ArmorClass)) // check for item of type ArmorClass
                    {
                        ArmorClass armorClass = (ArmorClass)item.Value; // cast item to ArmorClass

                        //Increase total primary attributes with values from armor
                        ranger.TotalPrimaryAttributes.Strength += armorClass.Attributes.Strength;
                        ranger.TotalPrimaryAttributes.Dexterity += armorClass.Attributes.Dexterity;
                        ranger.TotalPrimaryAttributes.Intelligence += armorClass.Attributes.Intelligence;

                    }

                }

                if (ranger.Level >= 1)
                {
                    // Increase total primary attributes with base primary attributes
                    ranger.TotalPrimaryAttributes.Strength += ranger.BasePrimaryAttributes.Strength;
                    ranger.TotalPrimaryAttributes.Dexterity += ranger.BasePrimaryAttributes.Dexterity;
                    ranger.TotalPrimaryAttributes.Intelligence += ranger.BasePrimaryAttributes.Intelligence;
                }


        }

        #endregion 
    }

}
    



    

