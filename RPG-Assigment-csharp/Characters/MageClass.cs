﻿using RPG_Assigment_csharp.Enums;
using RPG_Assigment_csharp.Exceptions;
using RPG_Assigment_csharp.Hero;
using RPG_Assigment_csharp.Items;
using RPG_Assigment_csharp.Stats;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Assigment_csharp.Characters
{
    public class MageClass : HeroClass
    {
        public double Damage { get; set; }

        private Dictionary<SlotEnum, ItemClass> inventory = new Dictionary<SlotEnum, ItemClass>();
        public Dictionary<SlotEnum, ItemClass> Inventory
        {
            get { return inventory; }
            private set { inventory = value; } // private set
        }

        public MageClass() : base()
        {

        }



        #region Equip item Method for Mage
        /// <summary>
        /// Equips item passed to method, required to pass in item, slot and instance of mage
        /// </summary>
        /// <param name="item">The item which is beeing passed</param>
        /// <param name="slot">Which slot to place item in </param>
        /// <param name="mage">Which instance of mage to equip</param>
        /// <returns>Message if equipping item was done or not</returns>
        /// <exception cref="InvalidArmorException">If not possible to equip that type of armor or beeing to low level for that armor</exception>
        /// <exception cref="InvalidWeaponException">If not possible to equip that type of weapon or beeing to low level for that weapon</exception>
        public string EquipMageItem(ItemClass item, SlotEnum slot, MageClass mage)
        {
            string notification = ""; // initialize variable 

             // check if inventory already contains item, if true remove slot before equipping new item
            if (Inventory.ContainsKey(slot) && slot.Equals(SlotEnum.SLOT_BODY)) 
            {
                Inventory.Remove(slot);
            }
            if (Inventory.ContainsKey(slot) && slot.Equals(SlotEnum.SLOT_LEGS))
            {
                Inventory.Remove(slot);
            }
            if (Inventory.ContainsKey(slot) && slot.Equals(SlotEnum.SLOT_HEAD))
            {
                Inventory.Remove(slot);
            }
            if (Inventory.ContainsKey(slot) && slot.Equals(SlotEnum.SLOT_WEAPON))
            {
                Inventory.Remove(slot);

            }

            if (item.GetType() == typeof(ArmorClass)) // check if item is of type ArmorClass
            {
                ArmorClass armor = (ArmorClass)item; // cast to ArmorClass
                if (GetType() == typeof(MageClass) && armor.ArmorType == ArmorEnum.ARMOR_CLOTH && mage.Level >= armor.ReqLvl) // Check if type is of MageClass, armor is cloth and level is higher or equal required level
                {
                    if (slot == SlotEnum.SLOT_WEAPON) // if slot is weapon, throw exception
                    {
                        throw new Exception("Not the right slot for an armor");
                    }
                    else
                    {
                        Inventory.Add(slot, item); // add item to slot
                        notification = "New armour equipped!";
                    }
                }
                else
                {
                    throw new InvalidArmorException("Not possible to equip this type of armor"); // throw custom exception
                }
                

            }

            if (item.GetType() == typeof(WeaponClass)) // check if item is of type WeaponClass
            {
                WeaponClass weapon = (WeaponClass)item; // cast item to Weapon
                //Check if requirements is met for equipping weapon
                if (GetType() == typeof(MageClass) && weapon.WeaponType.Equals(WeaponsEnum.WEAPON_STAFF) && mage.Level >= weapon.ReqLvl ||
                    GetType() == typeof(MageClass) && weapon.WeaponType.Equals(WeaponsEnum.WEAPON_WAND) && mage.Level >= weapon.ReqLvl)
                {
                    // If slot is not weapon
                    if (weapon.Slot != SlotEnum.SLOT_WEAPON)
                    {
                        throw new Exception("Not the right slot for a weapon");
                    }
                    else
                    {
                        Inventory.Add(slot, item); // add item to slot 
                        notification = "New weapon equipped!";
                    }
                }
                else
                {
                    throw new InvalidWeaponException("Not possible to equip this weapon"); // throw custom exception
                }

                
            }

            return notification;
        }


        #endregion


        #region Total variables with armor stats 
        /// <summary>
        /// For every item in mage.inventory add armor attributes to total stats
        /// if level is one add base stats
        /// </summary>
        /// <param name="mage">Which instance of mage is beeing passed in</param>
        /// <exception cref="Exception">Throws exception if wrong type is passed in</exception>
        public void TotalVariablesWithArmorStats(MageClass mage)
        {

                
                foreach (var item in mage.Inventory) // loop trough items in inventory
                {
                    if (item.Value.GetType() == typeof(ArmorClass)) // check for armor
                    {
                        ArmorClass armorClass = (ArmorClass)item.Value; // cast item to ArmorClass

                        //increase total primary attributes with armor attributes
                        mage.TotalPrimaryAttributes.Strength += armorClass.Attributes.Strength; 
                        mage.TotalPrimaryAttributes.Dexterity += armorClass.Attributes.Dexterity;
                        mage.TotalPrimaryAttributes.Intelligence += armorClass.Attributes.Intelligence;

                    }

                }
                if (mage.Level >= 1)
                {
                    // add primary attributes and base primary attributes togheter
                    mage.TotalPrimaryAttributes.Strength += mage.BasePrimaryAttributes.Strength;
                    mage.TotalPrimaryAttributes.Dexterity += mage.BasePrimaryAttributes.Dexterity;
                    mage.TotalPrimaryAttributes.Intelligence += mage.BasePrimaryAttributes.Intelligence;
                }

            


        }

        #endregion
    }

}
