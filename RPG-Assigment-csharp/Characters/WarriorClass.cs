﻿using RPG_Assigment_csharp.Enums;
using RPG_Assigment_csharp.Exceptions;
using RPG_Assigment_csharp.Hero;
using RPG_Assigment_csharp.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Assigment_csharp.Characters
{
    public class WarriorClass : HeroClass
    {
        public double Damage { get; set; }

        private Dictionary<SlotEnum, ItemClass> inventory = new Dictionary<SlotEnum, ItemClass>();
        public Dictionary<SlotEnum, ItemClass> Inventory
        {
            get { return inventory; }
            set { inventory = value; }
        }

        public WarriorClass() : base()
        {
        }

        #region Equip item Warrior method
        /// <summary>
        /// Equips item for warrior, required to pass item, slot and instance of warrior
        /// </summary>
        /// <param name="item">Which item is beeing passed in</param>
        /// <param name="slot">Wich slot is beeing passed in</param>
        /// <param name="warrior">Which instance is beeing passed in</param>
        /// <returns></returns>
        /// <exception cref="InvalidArmorException">If not possible to equip that type of armor or beeing to low level for that armor</exception>
        /// <exception cref="InvalidWeaponException">If not possible to equip that type of weapon or beeing to low level for that weapon</exception>
        public string EquipWarriorItem(ItemClass item, SlotEnum slot, WarriorClass warrior)
        {
            string notification = ""; // initialize variable with empty string

                //Check if slot equals slot and remove if there is an item equipped
                if (Inventory.ContainsKey(slot) && slot.Equals(SlotEnum.SLOT_BODY))
                {
                    Inventory.Remove(slot);
                }
                if (Inventory.ContainsKey(slot) && slot.Equals(SlotEnum.SLOT_LEGS))
                {
                    Inventory.Remove(slot);
                }
                if (Inventory.ContainsKey(slot) && slot.Equals(SlotEnum.SLOT_HEAD))
                {
                    Inventory.Remove(slot);
                }
                if (Inventory.ContainsKey(slot) && slot.Equals(SlotEnum.SLOT_WEAPON))
                {
                    Inventory.Remove(slot);

                }

                if (item.GetType() == typeof(ArmorClass)) // Check item type
                {
                    ArmorClass armor = (ArmorClass)item; // cast item to ArmorClass
                    
                    //Check requirements for equpping armor
                    if (GetType() == typeof(WarriorClass) && armor.ArmorType == ArmorEnum.ARMOR_PLATE && warrior.Level >= armor.ReqLvl ||
                       GetType() == typeof(WarriorClass) && armor.ArmorType == ArmorEnum.ARMOR_MAIL && warrior.Level >= armor.ReqLvl)
                    {
                        // if slot equals weapon, throw exception
                        if (slot == SlotEnum.SLOT_WEAPON)
                        {
                            throw new Exception("Not the right slot for an armor");
                        }
                        else // Add item to inventory
                        {   
                            Inventory.Add(slot, item);
                            notification = "New armour equipped!";
                        }
                        
                    }
                    else // throw exception if requirements not met
                    {
                        throw new InvalidArmorException("Not possible to equip this type of armor");
                    }
                }

                if (item.GetType() == typeof(WeaponClass)) // check item type
                {
                    WeaponClass weapon = (WeaponClass)item; // cast item to WeaponClass

                    // Check requirements for equipping weapon
                    if (GetType() == typeof(WarriorClass) && weapon.WeaponType == WeaponsEnum.WEAPON_SWORD && warrior.Level >= weapon.ReqLvl ||
                       GetType() == typeof(WarriorClass) && weapon.WeaponType == WeaponsEnum.WEAPON_HAMMER && warrior.Level >= weapon.ReqLvl ||
                       GetType() == typeof(WarriorClass) && weapon.WeaponType == WeaponsEnum.WEAPON_AXE && warrior.Level >= weapon.ReqLvl)
                    {
                        // if slot not equal to weapon, throw exception
                        if (weapon.Slot != SlotEnum.SLOT_WEAPON)
                        {
                            throw new Exception("Not the right slot for a weapon");
                        }
                        else // add item to inventory
                        {
                            Inventory.Add(slot, item);
                            notification = "New weapon equipped!";
                        }
                  
                    }
                    else // Throw exception if requirements not met
                    {
                        throw new InvalidWeaponException("Not possible to equip this weapon");
                    }

                    
            }
            return notification; // Return value 
        }
        #endregion


        #region Total variables with armor stats
        /// <summary>
        /// For every item in warrior.inventory add armor attributes to total stats,
        /// if level is one add base stats 
        /// </summary>
        /// <param name="warrior">Which instance of rogue is beeing passed in</param>
        /// <exception cref="Exception">Throws exception if wrong type is passed in</exception>
        public void TotalVariablesWithArmorStats(WarriorClass warrior)
        {
                
                foreach (var item in warrior.Inventory) // loop through inventory
                {
                    if (item.Value.GetType() == typeof(ArmorClass)) // check item type
                    {
                        ArmorClass armorClass = (ArmorClass)item.Value; // cast item to ArmorClass

                        // Add armor attributes to total primary attributes
                        warrior.TotalPrimaryAttributes.Strength += armorClass.Attributes.Strength;
                        warrior.TotalPrimaryAttributes.Dexterity += armorClass.Attributes.Dexterity;
                        warrior.TotalPrimaryAttributes.Intelligence += armorClass.Attributes.Intelligence;

                    }

                }

                if (warrior.Level >= 1)
                {   
                    // Add base values to total values
                    warrior.TotalPrimaryAttributes.Strength += warrior.BasePrimaryAttributes.Strength;
                    warrior.TotalPrimaryAttributes.Dexterity += warrior.BasePrimaryAttributes.Dexterity;
                    warrior.TotalPrimaryAttributes.Intelligence += warrior.BasePrimaryAttributes.Intelligence;
                }

            


        }

        #endregion
    }
}
