﻿using RPG_Assigment_csharp.Characters;
using RPG_Assigment_csharp.Enums;
using RPG_Assigment_csharp.Items;
using RPG_Assigment_csharp.Stats;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace RPG_Assigment_csharp.Hero
{
    public abstract class HeroClass 
    {
        private string name;
        private int level = 1;
        private BasePrimaryAttributes basePrimaryAttributes = new BasePrimaryAttributes();
        private TotalPrimaryAttributes totalPrimaryAttributes = new TotalPrimaryAttributes();
        
        public string Name 
        { 
            get { return name; } 
            set { name = value; } 
        }
        public BasePrimaryAttributes BasePrimaryAttributes 
        { 
            get { return basePrimaryAttributes; } 
            set { basePrimaryAttributes = value; } 
        }

        public TotalPrimaryAttributes TotalPrimaryAttributes
        {
            get { return totalPrimaryAttributes; }
            set { totalPrimaryAttributes = value; }

        }
        public int Level
        {
            get { return level; }
            private set { level = value; } // private set
        } 



        public HeroClass()
        {
            
            if(this.GetType() == typeof(MageClass)) // if mage created set base primary attributes
            {
                BasePrimaryAttributes.Strength = 1;
                BasePrimaryAttributes.Dexterity = 1;
                BasePrimaryAttributes.Intelligence = 8;
                
            }
            if (this.GetType() == typeof(RangerClass)) // if ranger created set base primary attributes
            {
                
                BasePrimaryAttributes.Strength = 1;
                BasePrimaryAttributes.Dexterity = 7;
                BasePrimaryAttributes.Intelligence = 1;
            }
            if (this.GetType() == typeof(RogueClass)) // if rogue created set base primary attributes
            {
                
                BasePrimaryAttributes.Strength = 2;
                BasePrimaryAttributes.Dexterity = 6;
                BasePrimaryAttributes.Intelligence = 1;
            }
            if (this.GetType() == typeof(WarriorClass)) // if warrior created set base primary attributes
            {
                
                BasePrimaryAttributes.Strength = 5;
                BasePrimaryAttributes.Dexterity = 2;
                BasePrimaryAttributes.Intelligence = 1;

            }

        }


        #region Poly Level up methods

        /// <summary>
        /// incrases level by 1 each time method is called
        /// </summary>
        /// <param name="mage">Mage parameter, pass the instantiated mage as paramter to level up that instance</param>
        /// <returns>returns string when leveled up</returns>
        public static string IncreaseLevel(MageClass mage)
        {

                mage.Level += 1; // Increase level and include previous values
                mage.TotalPrimaryAttributes.Strength += 1; // Increase strength and include previous values
                mage.TotalPrimaryAttributes.Dexterity += 1; // Increase Dexterity and include previous values
                mage.TotalPrimaryAttributes.Intelligence += 5; // Increase Intelligence and include previous values

            return "Level up";

        }

        /// <summary>
        /// incrases level by 1 each time method is called
        /// </summary>
        /// <param name="ranger">Ranger parameter, pass the instantiated ranger as paramter to level up that instance</param>
        /// <returns>returns string when leveled up</returns>
        public static string IncreaseLevel(RangerClass ranger)
        {

                ranger.Level += 1; // Increase level by 1 and include previous values
                ranger.TotalPrimaryAttributes.Strength += 1; // Increase Strength by 1 and include previous values
                ranger.TotalPrimaryAttributes.Dexterity += 5; // Increase Dexterity by 5 and include previous values
                ranger.TotalPrimaryAttributes.Intelligence += 1; // Increase Intelligence by 1 and include previous values

            return "Level up";
        }

        /// <summary>
        /// incrases level by 1 each time method is called
        /// </summary>
        /// <param name="rogue">Rogue parameter, pass the instantiated rogue as parameter to level up that instance</param>
        /// <returns>returns string when leveled up</returns>
        public static string IncreaseLevel(RogueClass rogue)
        {

                rogue.Level += 1; // Increase level by 1 and include previous values
                rogue.TotalPrimaryAttributes.Strength += 1; // Increase Strength by 1 and include previous values
                rogue.TotalPrimaryAttributes.Dexterity += 4; // Increase Dexterity by 4 and include previous values
                rogue.TotalPrimaryAttributes.Intelligence += 1; // Increase Intelligence by 1 and include previous values

            return "Level up";

        }

        /// <summary>
        /// incrases level by 1 each time method is called
        /// </summary>
        /// <param name="warrior">Warrior parameter, pass the instantiated warrior as parameter to level up that instance</param>
        /// <returns>returns string when leveled up</returns>
        public static string IncreaseLevel(WarriorClass warrior)
        {

                warrior.Level += 1; // Increase Level by 1 and include previous value
                warrior.TotalPrimaryAttributes.Strength += 3; // Increase Strength by 3 and include previous value
                warrior.TotalPrimaryAttributes.Dexterity += 2; // Increase Dexterity by 2 and include previous value
                warrior.TotalPrimaryAttributes.Intelligence += 1; // Increase Intelligence by 1 and include previous value

            return "Level up";

        }

        #endregion


        #region Poly Damage methods
        /// <summary>
        /// Caluclates damage of mage which is passed in as parameter
        /// </summary>
        /// <param name="mage"></param>
        /// <returns>Returns the damage calculated if there is a weapon and if there is no weapon</returns>
        public static double DamageCalculated(MageClass mage)
        {
            double damage = 0; // Initialize variable

            foreach (var item in mage.Inventory) // loop through each item in inventory
            {

                if (item.Value.GetType() == typeof(WeaponClass)) // If weapon is in inventory
                {
                        WeaponClass weaponClass = (WeaponClass)item.Value; // cast item to weapon
                        var dps = weaponClass.WeaponAttributes.Damage * weaponClass.WeaponAttributes.AttackSpeed; // Calculate dps
                        damage = dps * (1 + mage.totalPrimaryAttributes.Intelligence / 100); // calculate damage
                }

            }
            if (mage.Inventory.Count == 0 || // If inventory is empty
                 mage.Inventory.ContainsKey(SlotEnum.SLOT_BODY) && !mage.Inventory.ContainsKey(SlotEnum.SLOT_WEAPON) || // If there is a slot in body and no weapon
                 mage.Inventory.ContainsKey(SlotEnum.SLOT_HEAD) && !mage.Inventory.ContainsKey(SlotEnum.SLOT_WEAPON) || // if there is a slot in head and no weapon
                 mage.Inventory.ContainsKey(SlotEnum.SLOT_LEGS) && !mage.Inventory.ContainsKey(SlotEnum.SLOT_WEAPON)) // if there is a slot in legs and no weapon
            {
                damage = 1 * (1 + mage.totalPrimaryAttributes.Intelligence / 100); // set damage 
            }

            return damage; // return damage

        }

        /// <summary>
        /// Caluclates damage of ranger which is passed in as parameter
        /// </summary>
        /// <param name="ranger"></param>
        /// <returns>Returns the damage calculated if there is a weapon and if there is no weapon</returns>
        public static double DamageCalculated(RangerClass ranger)
        {
            double damage = 0; // Initialize damage variable

            foreach (var item in ranger.Inventory) // Loop through each item in inventory
            {

                if (item.Value.GetType() == typeof(WeaponClass)) // Checks if item is of type WeaponClass
                {
                    WeaponClass weaponClass = (WeaponClass)item.Value; // casts item to WeaponClass
                    var dps = weaponClass.WeaponAttributes.Damage * weaponClass.WeaponAttributes.AttackSpeed; // calculate dps
                    damage = dps * (1 + ranger.totalPrimaryAttributes.Dexterity / 100); // calculate damage
                }
               

            }
            if (ranger.Inventory.Count == 0 || // if inventory is empty
                 ranger.Inventory.ContainsKey(SlotEnum.SLOT_BODY) && !ranger.Inventory.ContainsKey(SlotEnum.SLOT_WEAPON) || // checks if inventory contains body slot and no weapon
                 ranger.Inventory.ContainsKey(SlotEnum.SLOT_HEAD) && !ranger.Inventory.ContainsKey(SlotEnum.SLOT_WEAPON) || // checks if inventory contains head slot and no weapon
                 ranger.Inventory.ContainsKey(SlotEnum.SLOT_LEGS) && !ranger.Inventory.ContainsKey(SlotEnum.SLOT_WEAPON)) // checks if inventory contains legs slot and no weapon
            {
                damage = 1 * (1 + ranger.totalPrimaryAttributes.Dexterity / 100); // set damage
            }
            return damage; // return damage

        }

        /// <summary>
        /// Caluclates damage of rogue which is passed in as parameter
        /// </summary>
        /// <param name="rogue"></param>
        /// <returns>Returns the damage calculated if there is a weapon and if there is no weapon</returns>
        public static double DamageCalculated(RogueClass rogue)
        {
            double damage = 0; // initialize damage variable

            foreach (var item in rogue.Inventory) // loop through inventory
            {

                if (item.Value.GetType() == typeof(WeaponClass)) // check if item is of type WeaponClass
                {
                    WeaponClass weaponClass = (WeaponClass)item.Value; // cast item to WeaponClass
                    var dps = weaponClass.WeaponAttributes.Damage * weaponClass.WeaponAttributes.AttackSpeed; // calculate dps
                    damage = dps * (1 + rogue.totalPrimaryAttributes.Dexterity / 100); // calculate damage

                }
               
            }
            if (rogue.Inventory.Count == 0 || // Checks if inventory is empty
                 rogue.Inventory.ContainsKey(SlotEnum.SLOT_BODY) && !rogue.Inventory.ContainsKey(SlotEnum.SLOT_WEAPON) || // checks if inventory contains body slot and no weapon
                 rogue.Inventory.ContainsKey(SlotEnum.SLOT_HEAD) && !rogue.Inventory.ContainsKey(SlotEnum.SLOT_WEAPON) || // checks if inventory contains head slot and no weapon
                 rogue.Inventory.ContainsKey(SlotEnum.SLOT_LEGS) && !rogue.Inventory.ContainsKey(SlotEnum.SLOT_WEAPON)) // checks if inventory contains legs slot and no weapon
            {
                damage = 1 * (1 + rogue.totalPrimaryAttributes.Dexterity / 100); // calculate damage
            }
            return damage; // return damage

        }

        /// <summary>
        /// Caluclates damage of warrior which is passed in as parameter
        /// </summary>
        /// <param name="warrior"></param>
        /// <returns>Returns the damage calculated if there is a weapon and if there is no weapon</returns>
        public static double DamageCalculated(WarriorClass warrior)
        {
            double damage = 0;  // initialize damage variable

            foreach (var item in warrior.Inventory) // loop through inventory
            {
                 
                if(item.Value.GetType() == typeof(WeaponClass)) // check if item is of type WeaponClass
                {
                   WeaponClass weaponClass = (WeaponClass)item.Value; // cast item to WeaponClass
                    var dps = weaponClass.WeaponAttributes.Damage * weaponClass.WeaponAttributes.AttackSpeed; // calculate dps
                    damage = dps * (1 + warrior.totalPrimaryAttributes.Strength / 100); // calculate damage
                }
               
            }
            if (warrior.Inventory.Count == 0 || // Checks if inventory is empty
                warrior.Inventory.ContainsKey(SlotEnum.SLOT_BODY) && !warrior.Inventory.ContainsKey(SlotEnum.SLOT_WEAPON) || // checks if inventory contains body slot and no weapon
                warrior.Inventory.ContainsKey(SlotEnum.SLOT_HEAD) && !warrior.Inventory.ContainsKey(SlotEnum.SLOT_WEAPON) || // checks if inventory contains head slot and no weapon
                warrior.Inventory.ContainsKey(SlotEnum.SLOT_LEGS) && !warrior.Inventory.ContainsKey(SlotEnum.SLOT_WEAPON)) // checks if inventory contains legs slot and no weapon
            {
                damage = 1 * (1 + warrior.totalPrimaryAttributes.Strength / 100);  // calculate damage
            }
            
            return damage; // return damage

        }


        #endregion
    }
}
