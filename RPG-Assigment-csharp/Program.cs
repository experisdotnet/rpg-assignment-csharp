﻿using RPG_Assigment_csharp.Characters;
using RPG_Assigment_csharp.Enums;
using RPG_Assigment_csharp.Hero;
using RPG_Assigment_csharp.Items;
using RPG_Assigment_csharp.Stats;
using System;


namespace RPG_Assigment_csharp
{
    class Program
    {
        static void Main(string[] args)
        {
            // Create example mage
            // Set name
            MageClass mage = new MageClass()
            {
                Name = "A cool Mage",
            };

            //Create example weapon
            WeaponClass staff = new WeaponClass()
            {
                Name = "A staff",
                ReqLvl = 1,
                Slot = SlotEnum.SLOT_WEAPON,
                WeaponAttributes = new WeaponAttrClass(){ Damage = 30, AttackSpeed = 1 },
                WeaponType = WeaponsEnum.WEAPON_STAFF
            };

            //Create example body armor
            ArmorClass clothBody = new ArmorClass()
            {
                Name = "Cloth body armor",
                ArmorType = ArmorEnum.ARMOR_CLOTH,
                Attributes = new ArmorAttrClass() { Dexterity = 1, Intelligence = 10, Strength = 1 },
                ReqLvl = 1,
                Slot = SlotEnum.SLOT_BODY,
            };

            //Equip items
            mage.EquipMageItem(staff, SlotEnum.SLOT_WEAPON, mage);
            mage.EquipMageItem(clothBody, SlotEnum.SLOT_BODY, mage);

            //Increase level of mage two times
            MageClass.IncreaseLevel(mage);
            MageClass.IncreaseLevel(mage);


            //Get total variables, call this method last
            //do your increase level and equip items before calling this method, call this method once
            mage.TotalVariablesWithArmorStats(mage);

            //Print mage
            PrintCharacter.PrintMage(mage);





            //Create example warrior
            WarriorClass warrior = new WarriorClass();

            ArmorClass plateBody = new ArmorClass()
            {
                Name = "Plate body armor",
                ArmorType = ArmorEnum.ARMOR_PLATE,
                Attributes = new ArmorAttrClass() { Dexterity = 1, Intelligence = 1, Strength = 10 },
                ReqLvl = 1,
                Slot = SlotEnum.SLOT_BODY,
            };

            warrior.EquipWarriorItem(plateBody, SlotEnum.SLOT_BODY, warrior);
            //Calculate total variables.
            //Note that this method must be called in order to calculate the stats and called only once
            warrior.TotalVariablesWithArmorStats(warrior);

            //Prints stats
            PrintCharacter.PrintWarrior(warrior);
        }

    }
}
