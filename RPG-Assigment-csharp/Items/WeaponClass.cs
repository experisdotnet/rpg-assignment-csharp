﻿using RPG_Assigment_csharp.Enums;
using RPG_Assigment_csharp.Stats;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Assigment_csharp.Items
{
    public class WeaponClass : ItemClass // Weapon class 
    {
        public WeaponAttrClass WeaponAttributes { get; set; }
        public WeaponsEnum WeaponType { get; set; }

    }


}
