﻿using RPG_Assigment_csharp.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Assigment_csharp.Items
{
    public abstract class ItemClass // Base class items
    {

        public string Name { get; set; }
        public int ReqLvl { get; set; }
        public SlotEnum? Slot { get; set; }

        

    }
}
