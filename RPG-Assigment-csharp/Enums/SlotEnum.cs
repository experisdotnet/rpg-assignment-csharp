﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Assigment_csharp.Enums
{
    public enum SlotEnum // List of slots
    {
        SLOT_WEAPON,
        SLOT_BODY,
        SLOT_HEAD,
        SLOT_LEGS
    }
}
