﻿using System;
using System.Text;
using RPG_Assigment_csharp.Characters;

public class PrintCharacter
{

    /// <summary>
    /// Prints stats for mage
    /// </summary>
    /// <param name="mage">instance of mage which is required to pass in when calling method</param>
    public static void PrintMage(MageClass mage)
    {
        // Stringbuilder which builds string and prints out information when called
        StringBuilder sb = new StringBuilder();
        sb.AppendLine("This is the current Mage stats");
        sb.AppendLine("*******************************************");
        sb.AppendLine("Name: " + mage.Name);
        sb.AppendLine("Level: " + mage.Level);
        sb.AppendLine("Strength: " + mage.TotalPrimaryAttributes.Strength);
        sb.AppendLine("Dexterity: " + mage.TotalPrimaryAttributes.Dexterity);
        sb.AppendLine("Intelligence: " + mage.TotalPrimaryAttributes.Intelligence);
        sb.AppendLine("Damage: " + MageClass.DamageCalculated(mage));
        sb.AppendLine("********************************************");
        Console.WriteLine(sb);

    }


    /// <summary>
    /// Prints stats for ranger
    /// </summary>
    /// <param name="ranger">instance of ranger which is required to pass in when calling method</param>
    public static void PrintRanger(RangerClass ranger)
    {
        // Stringbuilder which builds string and prints out information when called
        StringBuilder sb = new StringBuilder();
        sb.AppendLine("This is the current Ranger stats");
        sb.AppendLine("*******************************************");
        sb.AppendLine("Name: " + ranger.Name);
        sb.AppendLine("Level: " + ranger.Level);
        sb.AppendLine("Strength: " + ranger.TotalPrimaryAttributes.Strength);
        sb.AppendLine("Dexterity: " + ranger.TotalPrimaryAttributes.Dexterity);
        sb.AppendLine("Intelligence: " + ranger.TotalPrimaryAttributes.Intelligence);
        sb.AppendLine("Damage: " + RangerClass.DamageCalculated(ranger));
        sb.AppendLine("********************************************");
        Console.WriteLine(sb);

    }

    /// <summary>
    /// Prints stats for rogue
    /// </summary>
    /// <param name="rogue">instance of rogue which is required to pass in when calling method</param>
    public static void PrintRogue(RogueClass rogue)
    {
        // Stringbuilder which builds string and prints out information when called
        StringBuilder sb = new StringBuilder();
        sb.AppendLine("This is the current Rogue stats");
        sb.AppendLine("*******************************************");
        sb.AppendLine("Name: " + rogue.Name);
        sb.AppendLine("Level: " + rogue.Level);
        sb.AppendLine("Strength: " + rogue.TotalPrimaryAttributes.Strength);
        sb.AppendLine("Dexterity: " + rogue.TotalPrimaryAttributes.Dexterity);
        sb.AppendLine("Intelligence: " + rogue.TotalPrimaryAttributes.Intelligence);
        sb.AppendLine("Damage: " + RogueClass.DamageCalculated(rogue));
        sb.AppendLine("********************************************");
        Console.WriteLine(sb);

    }

    /// <summary>
    /// Prints stats for warrior
    /// </summary>
    /// <param name="warrior">instance of warrior which is required to pass in when calling method</param>
    public static void PrintWarrior(WarriorClass warrior)
    {
        // Stringbuilder which builds string and prints out information when called
        StringBuilder sb = new StringBuilder();
        sb.AppendLine("This is the current Warrior stats");
        sb.AppendLine("*******************************************");
        sb.AppendLine("Name: " + warrior.Name);
        sb.AppendLine("Level: " + warrior.Level);
        sb.AppendLine("Strength: " + warrior.TotalPrimaryAttributes.Strength);
        sb.AppendLine("Dexterity: " + warrior.TotalPrimaryAttributes.Dexterity);
        sb.AppendLine("Intelligence: " + warrior.TotalPrimaryAttributes.Intelligence);
        sb.AppendLine("Damage: " + WarriorClass.DamageCalculated(warrior));
        sb.AppendLine("********************************************");
        Console.WriteLine(sb);

    }

    public PrintCharacter()
	{
	}
}
