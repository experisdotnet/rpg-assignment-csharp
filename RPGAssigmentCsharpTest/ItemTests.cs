﻿using RPG_Assigment_csharp.Characters;
using RPG_Assigment_csharp.Enums;
using RPG_Assigment_csharp.Exceptions;
using RPG_Assigment_csharp.Items;
using RPG_Assigment_csharp.Stats;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGAssigmentCsharpTest
{
    public class ItemTests
    {
        #region To High level weapon test
        [Fact]
        public void WarriorConstructorAndWeaponClassConstructor_InitializeWithNewWeapon_ExpectedToThrowInvalidWeaponException()
        {
            //Arrange
            WarriorClass warrior = new WarriorClass();
            WeaponClass axe = new WeaponClass()
            {
                Name = "Hatchet",
                ReqLvl = 2,
                Slot = SlotEnum.SLOT_WEAPON,
                WeaponType = WeaponsEnum.WEAPON_AXE,
                WeaponAttributes = new WeaponAttrClass() { Damage = 10, AttackSpeed = 0.3 }

            };

            //Act & Assert
            Assert.Throws<InvalidWeaponException>(() => warrior.EquipWarriorItem(axe, SlotEnum.SLOT_WEAPON, warrior));
        }

        #endregion

        #region To High level armor test
        [Fact]
        public void WarriorConstructorAndArmorClassConstructor_InstantiateArmorAndWarrior_ExpectedToThrowInvalidArmorException()
        {
            // Arrange
            WarriorClass warrior = new WarriorClass();
            ArmorClass armor = new ArmorClass()
            {
                ArmorType = ArmorEnum.ARMOR_PLATE,
                Attributes = new ArmorAttrClass() { Dexterity = 1, Intelligence = 2, Strength = 4 },
                Name = "Plate piece",
                ReqLvl = 2,
                Slot = SlotEnum.SLOT_BODY
            };

            // Act and Assert
            Assert.Throws<InvalidArmorException>(() => warrior.EquipWarriorItem(armor, SlotEnum.SLOT_BODY, warrior));

        }


        #endregion

        #region Wrong type of Weapon test

        [Fact]
        public void WarriorClassConstructorAndWeaponClassConstructor_InstantiateWarriorAndBow_ExpectedToThrowInvalidWeaponException()
        {
            //Arrange
            WarriorClass warrior = new WarriorClass();
            WeaponClass bow = new WeaponClass()
            {
                Name = "Bow of Agility",
                ReqLvl = 1,
                Slot = SlotEnum.SLOT_WEAPON,
                WeaponAttributes = new WeaponAttrClass() { Damage = 10, AttackSpeed = 0.2 },
                WeaponType = WeaponsEnum.WEAPON_BOW
            };

            //Act and Assert
            Assert.Throws<InvalidWeaponException>(() => warrior.EquipWarriorItem(bow, SlotEnum.SLOT_WEAPON, warrior));
        }


        #endregion

        #region Wrong type of armor test
        [Fact]
        public void WarriorConstructorAndArmorConstructor_InstantiateWarriorAndClothArmor_ExpectedToThrowInvalidArmorException()
        {
            //Arrange
            WarriorClass warrior = new WarriorClass();
            ArmorClass clothArmor = new ArmorClass()
            {
                ArmorType = ArmorEnum.ARMOR_CLOTH,
                Attributes = new ArmorAttrClass() { Dexterity = 0, Intelligence = 11, Strength = 1 },
                Name = "Piece of Cloth",
                ReqLvl = 1,
                Slot = SlotEnum.SLOT_BODY
            };

            //Act and Assert
            Assert.Throws<InvalidArmorException>(() => warrior.EquipWarriorItem(clothArmor, SlotEnum.SLOT_BODY, warrior));

        }


        #endregion

        #region message on weapon equipped
        [Fact]
        public void WarriorConstructorAndWeaponConstructorAndEquipItemWarrior_InstantiateWarriorAndWeaponAndAddWeaponToInventory_ExpectedShouldReturnSuccessMessage()
        {
            //Arrange
            string message = "New weapon equipped!";
            string expected = message;
            WarriorClass warrior = new WarriorClass();
            WeaponClass hammer = new WeaponClass()
            {
                Name = "Big ol hammer",
                ReqLvl = 1,
                Slot = SlotEnum.SLOT_WEAPON,
                WeaponAttributes = new WeaponAttrClass() { Damage = 15, AttackSpeed = 0.05 },
                WeaponType = WeaponsEnum.WEAPON_HAMMER
            };

            //Act
            string actual = warrior.EquipWarriorItem(hammer, SlotEnum.SLOT_WEAPON, warrior);

            //Assert
            Assert.Equal(expected, actual);


        }


        #endregion

        #region message on armor equipped
        [Fact]
        public void WarriorConstructorAndArmorConstructorAndEquipWarriorItem_InstantiateWarriorAndArmorAndEquipWeapon_ExpectedShouldEqualSuccessMessage()
        {
            //Arrange
            WarriorClass warrior = new WarriorClass();
            ArmorClass plateArmor = new ArmorClass()
            {
                ArmorType = ArmorEnum.ARMOR_PLATE,
                Attributes = new ArmorAttrClass() { Dexterity = 1, Intelligence = 2, Strength = 15 },
                Name = "Massive plate armor",
                ReqLvl = 1,
                Slot = SlotEnum.SLOT_BODY
            };

            string message = "New armour equipped!";
            string expected = message;

            // Act
            string actual = warrior.EquipWarriorItem(plateArmor, SlotEnum.SLOT_BODY, warrior);

            //Assert
            Assert.Equal(expected, actual);


        }


        #endregion

        #region Expected damage if no weapon is equipped
        [Fact]
        public void WarriorConstructorAndCalculateDamageMethod_InstantiateWarriorClassAndUseCalculateDamageMethodOnLvl1Warrior_ExpectedVariableShouldEqualWarriorDamage()
        {
            //arrange
            WarriorClass warrior = new WarriorClass();
            
            double sum = 1 * (1 + (5 / 100));
            double expected = sum;

            //act
            double actual = WarriorClass.DamageCalculated(warrior);

            //Assert
            Assert.Equal(expected, actual);    

        }


        #endregion

        #region Expected damage if valid weapon is equipped
        [Fact]
        public void WarriorConstructorAndWeaponConstructorAndCalculateWarriorDamageMethod_InstantiateConstructorsAndSetActualValueToResultOfCalculatedDamageMethod_ExpectedValueShouldBeEqualActualValue()
        {
            //Arrange
            WarriorClass warrior = new WarriorClass();
            
            WeaponClass axe = new WeaponClass()
            {
                Name = "Small Hatchet",
                ReqLvl = 1,
                Slot = SlotEnum.SLOT_WEAPON,
                WeaponAttributes = new WeaponAttrClass() { Damage = 7, AttackSpeed = 1.1 },
                WeaponType = WeaponsEnum.WEAPON_AXE
            };
            double sum = (7 * 1.1) * (1 + (5 / 100));
            double expected = sum;

            //Act
            warrior.EquipWarriorItem(axe, SlotEnum.SLOT_WEAPON, warrior);
            double actual = WarriorClass.DamageCalculated(warrior);

            //Assert
            Assert.Equal(expected, actual);

        }


        #endregion

        #region Expected damage if valid weapon and armor equipped
        [Fact]
        public void WarriorConstructorAndArmorConstructorAndWeaponConstructor_InstantiateAllThreeConstructors_ExpectedValueShouldEqualActualValue()
        {
            //Arrange
            WarriorClass warrior = new WarriorClass();
            
            WeaponClass axe = new WeaponClass()
            {
                Name = "Rusty Hatchet",
                ReqLvl = 1,
                Slot = SlotEnum.SLOT_WEAPON,
                WeaponAttributes = new WeaponAttrClass() { Damage = 7, AttackSpeed = 1.1 },
                WeaponType = WeaponsEnum.WEAPON_AXE
            };
            ArmorClass plateArmor = new ArmorClass()
            {
                ArmorType = ArmorEnum.ARMOR_PLATE,
                Attributes = new ArmorAttrClass() { Dexterity = 1, Intelligence = 1, Strength = 10 },
                Name = "Small Rounded Hatchet",
                ReqLvl = 1,
                Slot = SlotEnum.SLOT_WEAPON
            };

            double sum = (7 * 1.1) * (1 + ((5 + 1) / 100));
            double expected = sum;

            //Act
            warrior.EquipWarriorItem(axe, SlotEnum.SLOT_WEAPON, warrior);
            warrior.EquipWarriorItem(plateArmor, SlotEnum.SLOT_BODY, warrior);
            double actual = WarriorClass.DamageCalculated(warrior);

            //Assert
            Assert.Equal(expected, actual);

        }


        #endregion

    }
}
