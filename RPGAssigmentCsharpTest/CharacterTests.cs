﻿using RPG_Assigment_csharp.Characters;
using RPG_Assigment_csharp.Stats;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGAssigmentCsharpTest
{
    public class CharacterTests
    {
        #region Character Constructor Initialization without setting level
        [Fact]
        public void MageConstructor_InitializeWithoutSettingLevel_ShouldReturnOne()
        {
            //Arrange
            int level = 1;
            int expected = level;

            //Act
            MageClass mage = new MageClass();
            int actual = mage.Level;

            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void RangerConstructor_InitializeWithoutSettingLevel_ShouldReturnOne()
        {
            //Arrange
            int level = 1;
            int expected = level;

            //Act
            RangerClass ranger = new RangerClass();
            int actual = ranger.Level;

            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void RogueConstructor_InitializeWithoutSettingLevel_ShouldReturnOne()
        {
            //Arrange
            int level = 1;
            int expected = level;

            //Act
            RogueClass rogue = new RogueClass();
            int actual = rogue.Level;

            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void WarriorConstructor_InitializeWithoutSettingLevel_ShouldReturnOne()
        {
            //Arrange
            int level = 1;
            int expected = level;

            //Act
            WarriorClass warrior = new WarriorClass();
            int actual = warrior.Level;

            //Assert
            Assert.Equal(expected, actual);
        }


        #endregion

        #region Character level gain
        [Fact]
        public void MageIncreaseLevel_InstantiateMageClassWithProperConstructorsAndIncreaseLevelByOneAfterCallingMethod_ShouldReturnTwo()
        {
            //Act
            int level = 2;
            int expected = level;

            //Arrange
            MageClass mage = new MageClass();
            
            MageClass.IncreaseLevel(mage);
            int actual = mage.Level;

            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void RangerIncreaseLevel_InstantiateRangerClassWithProperConstructorsAndIncreaseLevelByOneAfterCallingMethod_ShouldReturnTwo()
        {
            //Act
            int level = 2;
            int expected = level;

            //Arrange
            RangerClass ranger = new RangerClass();
            
            RangerClass.IncreaseLevel(ranger);
            int actual = ranger.Level;

            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void RogueIncreaseLevel_InstantiateRogueClassWithProperConstructorsAndIncreaseLevelByOneAfterCallingMethod_ShouldReturnTwo()
        {
            //Act
            int level = 2;
            int expected = level;

            //Arrange
            RogueClass rogue = new RogueClass();
            
            RogueClass.IncreaseLevel(rogue);
            int actual = rogue.Level;

            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void WarriorIncreaseLevel_InstantiateWarriorClassWithProperConstructorsAndIncreaseLevelByOneAfterCallingMethod_ShouldReturnTwo()
        {
            //Act
            int level = 2;
            int expected = level;

            //Arrange
            WarriorClass warrior = new WarriorClass();
           
            WarriorClass.IncreaseLevel(warrior);
            int actual = warrior.Level;

            //Assert
            Assert.Equal(expected, actual);
        }




        #endregion

        #region Each character class is created wtih the proper default attributes

        [Fact]
        public void MageConstructorWithBasePrimaryAttributesConstructorSet_SetBasePrimaryAttributesConstructor_ShouldReturnSumEquals10()
        {
            //Arrange
            double sum = 10;
            double expected = sum;

            //Act
            MageClass mage = new MageClass();
            
            double actual = mage.BasePrimaryAttributes.Intelligence + mage.BasePrimaryAttributes.Dexterity + mage.BasePrimaryAttributes.Strength;

            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void RangerConstructorWithBasePrimaryAttributesConstructorSet_SetBasePrimaryAttributesConstructor_ShouldReturnSumEquals9()
        {
            //Arrange
            double sum = 9;
            double expected = sum;

            //Act
            RangerClass ranger = new RangerClass();
            
            double actual = ranger.BasePrimaryAttributes.Intelligence + ranger.BasePrimaryAttributes.Dexterity + ranger.BasePrimaryAttributes.Strength;

            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void RogueConstructorWithBasePrimaryAttributesConstructorSet_SetBasePrimaryAttributesConstructor_ShouldReturnSumEquals9()
        {
            //Arrange
            double sum = 9;
            double expected = sum;

            //Act
            RogueClass rogue = new RogueClass();
            
            double actual = rogue.BasePrimaryAttributes.Intelligence + rogue.BasePrimaryAttributes.Dexterity + rogue.BasePrimaryAttributes.Strength;

            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void WarriorConstructorWithBasePrimaryAttributesConstructorSet_SetBasePrimaryAttributesConstructor_ShouldReturnSumEquals8()
        {
            //Arrange
            double sum = 8;
            double expected = sum;

            //Act
            WarriorClass warrior = new WarriorClass();
            
            double actual = warrior.BasePrimaryAttributes.Intelligence + warrior.BasePrimaryAttributes.Dexterity + warrior.BasePrimaryAttributes.Strength;

            //Assert
            Assert.Equal(expected, actual);
        }

        #endregion

        #region Each character class has their attributes increased when levling up

        [Fact]
        public void GetTotalVariablesWithArmorStatsMageAndIncreaseMageLevel_CreateInstanceOfMageAndLevelUpAndGetTotalAttributes_ActualEquals17()
        {
            //Arrange
            double sum = 17;
            double expected = sum;

            //Act
            MageClass mage = new MageClass();
            
            mage.TotalVariablesWithArmorStats(mage);
            MageClass.IncreaseLevel(mage);

            double actual = mage.TotalPrimaryAttributes.Intelligence + mage.TotalPrimaryAttributes.Strength + mage.TotalPrimaryAttributes.Dexterity;

            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void GetTotalVariablesWithArmorStatsRangerAndIncreaseRangerLevel_CreateInstanceOfMageAndLevelUpAndGetTotalAttributes_ActualEquals16()
        {
            //Arrange
            double sum = 16;
            double expected = sum;

            //Act
            RangerClass ranger = new RangerClass();
            
            ranger.TotalVariablesWithArmorStats(ranger);
            RangerClass.IncreaseLevel(ranger);

            double actual = ranger.TotalPrimaryAttributes.Intelligence + ranger.TotalPrimaryAttributes.Strength + ranger.TotalPrimaryAttributes.Dexterity;

            //Assert
            Assert.Equal(expected, actual);
        }


        [Fact]
        public void GetTotalVariablesWithArmorStatsRogueAndIncreaseRogueLevel_CreateInstanceOfMageAndLevelUpAndGetTotalAttributes_ActualEquals15()
        {
            //Arrange
            double sum = 15;
            double expected = sum;

            //Act
            RogueClass rogue = new RogueClass();
            
            rogue.TotalVariablesWithArmorStats(rogue);
            RogueClass.IncreaseLevel(rogue);

            double actual = rogue.TotalPrimaryAttributes.Intelligence + rogue.TotalPrimaryAttributes.Strength + rogue.TotalPrimaryAttributes.Dexterity;

            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void GetTotalVariablesWithArmorStatsWarriorAndIncreaseWarriorLevel_CreateInstanceOfMageAndLevelUpAndGetTotalAttributes_ActualEquals14()
        {
            //Arrange
            double sum = 14;
            double expected = sum;

            //Act
            WarriorClass warrior = new WarriorClass();
            
            warrior.TotalVariablesWithArmorStats(warrior);
            WarriorClass.IncreaseLevel(warrior);

            double actual = warrior.TotalPrimaryAttributes.Intelligence + warrior.TotalPrimaryAttributes.Strength + warrior.TotalPrimaryAttributes.Dexterity;

            //Assert
            Assert.Equal(expected, actual);
        }


        #endregion
    }
}
