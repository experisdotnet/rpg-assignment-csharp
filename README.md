# RPG-Assignment-CSharp
An assigment which was handed out by noroff to pupils the summer of 2022.
The purpose of the assignment was to practice Object Oriented design to create classes, interfaces, inheritance between classes and create methods that fulfill a purpose for the application.

Testing was also included in a separate project inside same solution.


## Getting started
 - Clone this repository:
$ git clone https://gitlab.com/experisdotnet/rpg-assignment-csharp.git

- Navigate to solution:
cd yourpath/rpg-assignment-csharp/RPG-Assigment-csharp/RPG-Assigment-csharp.sln

- Start solution from terminal:
start yourpath/RPG-Assigment-csharp.sln

## Try it out
- Right click on the Test project and click run tests to start the tests. 

- Click F5 to start debugging to see the example code run in console.

## Description
A hero creator console application where you instantiate objects and use methods to equip items, increase the level of a character and calculate the damage of the character that has been created. 


## Tech
- XUnit 
- C# .NET 6
